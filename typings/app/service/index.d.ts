// This file is created by egg-ts-helper@1.25.6
// Do not modify this file!!!!!!!!!

import 'egg';
import ExportJwt = require('../../../app/service/jwt');
import ExportUser = require('../../../app/service/user');

declare module 'egg' {
  interface IService {
    jwt: ExportJwt;
    user: ExportUser;
  }
}
