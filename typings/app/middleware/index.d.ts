// This file is created by egg-ts-helper@1.25.6
// Do not modify this file!!!!!!!!!

import 'egg';
import ExportCors = require('../../../app/middleware/cors');

declare module 'egg' {
  interface IMiddleware {
    cors: typeof ExportCors;
  }
}
