// This file is created by egg-ts-helper@1.25.6
// Do not modify this file!!!!!!!!!

import 'egg';
import ExportHome = require('../../../app/model/home');

declare module 'egg' {
  interface IModel {
    Home: ReturnType<typeof ExportHome>;
  }
}
