'use strict';

const Service = require('egg').Service;

class UserService extends Service {

  /**
   * 根据id查询
   * @param id
   * @returns {Promise<{user: *}>}
   */
  async findById(id) {
    const user = await this.app.mysql.query(
      `select * from user where id = ${id}`,
      ''
    );
    return { user };
  }

  /**
   * 删除
   * @param id
   * @returns {Promise<Object>}
   */
  async deleteUsers(id) {
    const res = await this.app.mysql.delete('user', {
      id
    });
    return res;
  }

  /**
   * 登录
   * @param {*} username
   */
  async loginUsers(username) {
    let result;
    result = await this.app.mysql.get('user', { username: username });
    if (result == null) {
      let dto = {
        code: 0,
        mes: '查询不到信息',
        result: result
      };
      return { ...dto };
    } else {
      let dto = {
        code: 1,
        mes: '登录成功',
        result: result
      };
      return { ...dto };
    }
  }

  async getUserService (){
    let lists = await this.app.mysql.query(`select * from user`)
    return lists
  }

  /**
   * 注册
   * @param dto
   * @returns {Promise<Object>}
   */
  async realServiceRegister(dto) {
    let result;
    result = await this.app.mysql.insert('user', {
      username: dto.username,
      password: dto.password
    });
    return result;
  }
}

module.exports = UserService;
