'use strict';


/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { router, controller } = app;
  router.get('/getByUserId', controller.home.index);
  router.get('/loginIndex', controller.home.loginUser);
  router.delete('/delete', controller.home.deleteUser);
  router.post('/realLogin', app.jwt,controller.home.realRegisterUser);
  router.get('/getUsers',controller.home.getUsers)
};
