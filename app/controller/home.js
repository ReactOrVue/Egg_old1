'use strict';

const Controller = require('egg').Controller;

class HomeController extends Controller {
  async index() {
    const { ctx } = this;
    // eslint-disable-next-line no-unused-vars
    let id = '';
    if (ctx.query.id) {
      // eslint-disable-next-line no-const-assign
      id = ctx.query.id;
    } else {
      id = '00000000000000000000000000000001';
    }
    const user = await ctx.service.user.findById(id);
    ctx.body = user;
  }

  /**
   * 用户删除
   * @returns {Promise<void>}
   */
  async deleteUser() {
    const { ctx } = this;
    const id = ctx.request.body.id;
    const user = await ctx.service.user.deleteUsers(id);
    ctx.body = user;
  }

  /**
   * 用户登录
   * @returns {Promise<void>}
   */
  async loginUser() {
    const { ctx } = this;
    const login = await ctx.service.user.loginUsers(ctx.query.username);
    ctx.body = login;
  }

  async getUsers (){
    const {ctx} = this
    const lists = await ctx.service.user.getUserService()
    ctx.body = lists
  }

  /**
   * 用户注册
   * @returns {Promise<void>}
   */
  async realRegisterUser() {
    const { ctx } = this;
    let dto = {
      username: ctx.request.body.username,
      password: ctx.request.body.password
    };
    console.log('=========dto', dto);
    const register = await ctx.service.user.realServiceRegister(dto);
    ctx.body = register;
  }
}

module.exports = HomeController;
