// eslint-disable-next-line strict
module.exports = app => {
  const { STRING, INTEGER } = app.Sequelize

  const User = app.model.define('user', {
    id: {
      type: INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    username: STRING(255),
    password: STRING(255),
    sex: STRING(255),
  })

  return User;
}
