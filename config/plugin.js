'use strict'

/** @type Egg.EggPlugin */
// module.exports = {
//   // had enabled by egg
//   // static: {
//   //   enable: true,
//   // }
// };

'use strict'
exports.mysql = {
  enable: true,
  package: 'egg-mysql'
}


exports.jwt = {
  enable: true,
  package: "egg-jwt"
}

// exports.nunjucks = {
//   enable: true,
//   package: 'egg.view.nunjucks'
// }
