// /* eslint valid-jsdoc: "off" */

// 'use strict';

// /**
//  * @param {Egg.EggAppInfo} appInfo app info
//  */
// module.exports = appInfo => {
//   /**
//    * built-in config
//    * @type {Egg.EggAppConfig}
//    **/
//   const config = exports = {};

//   // use for cookie sign key, should change to your own and keep security
//   config.keys = appInfo.name + '_1566542791615_2143';

//   // add your middleware config here
//   config.middleware = [];

//   // add your user config here
//   const userConfig = {
//     // myAppName: 'egg',
//   };

//   return {
//     ...config,
//     ...userConfig,
//   };
// };
// const database = 'egg' // 数据库名字
// module.exports = appInfo => {
//   const config = {}
//   config.keys = appInfo.name + '_1501817502166_7037'

//   config.sequelize = {
//     // egg-sequelize 配置
//     dialect: 'mysql', // db type
//     database: 'egg',
//     host: 'localhost',
//     port: '3306',
//     username: 'root',
//     password: '12345678'
//   }

//   return config
// }
'use strict'

module.exports = appInfo => {
  const config = (exports = {})
  config.keys = appInfo.name + '_1566542791615_2143'

  config.jwt = {
    secret: appInfo.name + '_1566542791615_2143'
  }

  // config.security={
  //   csrf :{
  //     enable: false
  //   }
  // }

  config.mysql = {
    client: {
      // host
      host: '127.0.0.1',
      // 端口号
      port: '3306',
      // 用户名
      user: 'root',
      // 密码
      password: '12345678',
      // 数据库名
      database: 'egg'
    },
    // 是否加载到 app 上，默认开启
    app: true,
    // 是否加载到 agent 上，默认关闭
    agent: false
  }

  return {
    ...config
  }
}
